# class Breg



## Constants

## Properties
- `public $handlers = [];` Callbacks for functions defined in the regex

## Methods 
- `public function handle(string $function_name, callable $callable)` 
- `public function match($regex, $string)` Match 'better' regex against a string
- `public function parse($regex)` Parse the regex, then call all `::functions()` & fill in the regex with their results




