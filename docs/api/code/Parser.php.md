# class Breg\Parser



## Constants

## Properties
- `protected $patterns = [
        'comment'=>'/(?:^|\s)#[^\n]*$/m',
        'pad'=> '/(^\s*|\s*$)/m',
        'esc_trail_slash'=>'/(^|[^\\\\])((?:\\\\\\\\)*)(\\\\)$/m',
        'replace_esc_trail_slash'=>'$1$2\\ ',
    ];` 

## Methods 
- `public function parse($regex)` parse a regex string into an array
- `protected function removeComments($reg)` 
- `protected function removePad($reg)` 
- `protected function addEscapedTrailingSpace($reg)` 
- `protected function implodeLines($reg)` 
- `protected function cleanLine($regLine)` Each comment must start with ` #` (space hash). Everything following in that line is comment and will be removed from output
If your pattern uses ` #`, escape it like ` \#`

- `protected function parseRegFunc($line)` ```
::functionName(arg1 ;; arg2 ;; arg3) ## Comments if you want
```
- The function must be the only thing on the line. Comments allowed.  
- Args must **not** be quoted. Every arg will be trimmed (surrounding whitespace is removed).  
- Args are separated by ' ;; ' (space semicolon semicolon space)






